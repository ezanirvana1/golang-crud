package handler

import (
	"crud/platform/article"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ArticleFeedGet(feed *article.Repo) gin.HandlerFunc {
	return func(c *gin.Context) {
		results := feed.GetALl()
		c.JSON(http.StatusOK, results)
	}
}
