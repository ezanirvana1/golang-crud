package handler

import (
	"crud/platform/article"
	"net/http"

	"github.com/gin-gonic/gin"
)

type articleFeedPostRequest struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

func ArticleFeedPost(feed *article.Repo) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestBody := articleFeedPostRequest{}
		c.Bind(&requestBody)

		item := article.Item{
			Title: requestBody.Title,
			Body:  requestBody.Body,
		}
		feed.Add(item)

		c.Status(http.StatusNoContent)
	}
}
