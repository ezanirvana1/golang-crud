package main

import (
	"crud/platform/article"
	"crud/server/handler"

	"github.com/gin-gonic/gin"
)

func main()  {
	feed := article.New()
	
	r := gin.Default()

	r.GET("/home", handler.Home())
	r.GET("/article", handler.ArticleFeedGet(feed))
	r.POST("/article", handler.ArticleFeedPost(feed))
	
	r.Run()
}